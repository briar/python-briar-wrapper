# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

import pytest
from urllib.error import HTTPError, URLError

from briar_wrapper.api_thread import ApiThread

MODULE = 'briar_wrapper.api_thread.%s'

HEADLESS_JAR = 'briar-headless.jar'
PASSWORD = 'LjnM6/WPQ]V?@<=$'
CREDENTIALS = ('Alice', PASSWORD)


def test_is_running(api, process):
    process.poll.return_value = None
    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._process = process

    assert api_thread.is_running() is True


def test_is_running_none(api):
    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._process = None

    assert api_thread.is_running() is False


def test_is_running_poll_none(api, process):
    process.poll.return_value = 0
    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._process = process

    assert api_thread.is_running() is False


def test_start_already_running(api, is_running):
    api_thread = ApiThread(api, HEADLESS_JAR)

    with pytest.raises(Exception, match='API already running'):
        api_thread.start()


def test_start_init_popen(api, mocker):
    popen_mock = mocker.patch(MODULE % "Popen")
    api_thread = ApiThread(api, HEADLESS_JAR)

    api_thread.start()

    popen_mock.assert_called_once()


def test_watch_already_running(api, is_running, mocker):
    thread_mock = mocker.patch(MODULE % "Thread")
    api_thread = ApiThread(api, HEADLESS_JAR)

    api_thread.watch(None)

    thread_mock.assert_called_once()


def test_watch_init_thread(api, mocker):
    thread_mock = mocker.patch(MODULE % "Thread")
    api_thread = ApiThread(api, HEADLESS_JAR)

    api_thread.watch(None)

    thread_mock.assert_called_once()


def test_login_not_running(api):
    api_thread = ApiThread(api, HEADLESS_JAR)

    with pytest.raises(Exception, match="Can't login; API not running"):
        api_thread.login(PASSWORD)


def test_login_communicate(api, is_running, process):
    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._process = process

    api_thread.login(PASSWORD)

    process.communicate.assert_called_once_with(
        (PASSWORD + "\n").encode("utf-8")
    )


def test_register_not_running(api):
    api_thread = ApiThread(api, HEADLESS_JAR)

    with pytest.raises(Exception, match="Can't register; API not running"):
        api_thread.register(CREDENTIALS)


def test_register_communicate(api, is_running, process):
    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._process = process

    api_thread.register(CREDENTIALS)

    process.communicate.assert_called_once_with(
        (CREDENTIALS[0] + '\n' +
         CREDENTIALS[1] + '\n' +
         CREDENTIALS[1] + '\n').encode("utf-8")
    )


def test_stop(api, is_running, process):
    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._process = process

    api_thread.stop()

    process.terminate.assert_called_once()


def test_stop_not_running(api):
    api_thread = ApiThread(api, HEADLESS_JAR)

    with pytest.raises(Exception, match='Nothing to stop'):
        api_thread.stop()


def test_watch_thread(api, mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.side_effect = [True, False]

    urlopen_mock = mocker.patch(MODULE % "urlopen")
    sleep_mock = mocker.patch(MODULE % "sleep")

    callback_mock = mocker.MagicMock()

    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._watch_thread(callback_mock)

    assert is_running_mock.called is True
    urlopen_mock.assert_called_once_with("http://localhost:7000/v1/")
    callback_mock.assert_called_once_with(False)


def test_watch_thread_sleep(api, mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.side_effect = [True, False]

    urlopen_mock = mocker.patch(MODULE % "urlopen")
    sleep_mock = mocker.patch(MODULE % "sleep")

    callback_mock = mocker.MagicMock()

    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._watch_thread(callback_mock)

    sleep_mock.assert_called_once_with(0.1)


def test_watch_thread_not_running(api, mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.return_value = False

    urlopen_mock = mocker.patch(MODULE % "urlopen")

    callback_mock = mocker.MagicMock()

    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._watch_thread(callback_mock)

    is_running_mock.assert_called_once()
    assert urlopen_mock.called is False
    callback_mock.assert_called_once_with(False)


def test_watch_thread_404(api, mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.return_value = True

    urlopen_mock = mocker.patch(MODULE % "urlopen")
    urlopen_mock.side_effect = HTTPError(code=404, msg=None, hdrs=None,
                                         fp=None, url=None)

    callback_mock = mocker.MagicMock()

    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._watch_thread(callback_mock)

    api.on_successful_startup.assert_called_once_with(callback_mock)


def test_watch_thread_connection_refused(api, mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.side_effect = [True, False]

    urlopen_mock = mocker.patch(MODULE % "urlopen")
    urlopen_mock.side_effect = URLError(reason=ConnectionRefusedError())

    callback_mock = mocker.MagicMock()

    api_thread = ApiThread(api, HEADLESS_JAR)
    api_thread._watch_thread(callback_mock)

    callback_mock.assert_called_once_with(False)


def test_watch_thread_url_error(api, mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.return_value = True

    urlopen_mock = mocker.patch(MODULE % "urlopen")
    urlopen_mock.side_effect = URLError(reason=None)

    api_thread = ApiThread(api, HEADLESS_JAR)

    with pytest.raises(URLError):
        api_thread._watch_thread(None)


@pytest.fixture
def api(mocker):
    return mocker.MagicMock()


@pytest.fixture
def is_running(mocker):
    is_running_mock = mocker.patch(MODULE % 'ApiThread.is_running')
    is_running_mock.return_value = True
    return is_running_mock


@pytest.fixture
def process(mocker):
    return mocker.MagicMock()
