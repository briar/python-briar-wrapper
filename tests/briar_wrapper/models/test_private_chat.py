# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

import json

import requests_mock

from briar_wrapper.models.private_chat import PrivateChat

BASE_HTTP_URL = "http://localhost:7000/v1/messages/%s"

TEST_CONTACT_ID = 42
TEST_MESSAGE_ID = "+AIMMgOCPFF8HDEhiEHYjbfKrg7v0G94inKxjvjYzA8="
TEST_TEXT = "Hello World"


@requests_mock.Mocker(kw="requests_mock")
def test_get_empty(api, request_headers, requests_mock):
    private_chat = PrivateChat(api, TEST_CONTACT_ID)
    url = BASE_HTTP_URL % TEST_CONTACT_ID
    response = []

    requests_mock.register_uri("GET", url, request_headers=request_headers,
                               text=json.dumps(response))
    assert private_chat.get() == response


@requests_mock.Mocker(kw="requests_mock")
def test_send_message(api, request_headers, requests_mock):
    private_chat = PrivateChat(api, TEST_CONTACT_ID)
    url = BASE_HTTP_URL % TEST_CONTACT_ID

    requests_mock.register_uri("POST", url, request_headers=request_headers,
                               additional_matcher=match_request_send_message)
    private_chat.send(TEST_TEXT)


@requests_mock.Mocker(kw="requests_mock")
def test_delete_all_messages(api, request_headers, requests_mock):
    private_chat = PrivateChat(api, TEST_CONTACT_ID)
    url = BASE_HTTP_URL % f"{TEST_CONTACT_ID}/all"

    requests_mock.register_uri("DELETE", url, request_headers=request_headers)
    private_chat.delete_all_messages()


@requests_mock.Mocker(kw="requests_mock")
def test_mark_read(api, request_headers, requests_mock):
    private_chat = PrivateChat(api, TEST_CONTACT_ID)
    url = BASE_HTTP_URL % f"{TEST_CONTACT_ID}/read"

    requests_mock.register_uri("POST", url, request_headers=request_headers,
                               additional_matcher=match_request_mark_read)
    private_chat.mark_read(TEST_MESSAGE_ID)


def match_request_send_message(request):
    return {"text": TEST_TEXT} == request.json()


def match_request_mark_read(request):
    return {"messageId": TEST_MESSAGE_ID} == request.json()
