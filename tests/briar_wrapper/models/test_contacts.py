# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

import json

import pytest
import requests_mock
from briar_wrapper.exception import BriarWrapperException
from briar_wrapper.exceptions.pending_already_exists_contact import \
    PendingContactAlreadyExistsContact
from briar_wrapper.exceptions.pending_already_exists_pending_contact import \
    PendingContactAlreadyExistsPendingContact
from briar_wrapper.exceptions.pending_invalid_link import \
    PendingContactInvalidLinkException
from briar_wrapper.exceptions.pending_invalid_public_key import \
    PendingContactInvalidPublicKeyException

from briar_wrapper.models.contacts import Contacts
from briar_wrapper.models.socket_listener import SocketListener

BASE_HTTP_URL = "http://localhost:7000/v1/contacts/"

TEST_ALIAS = "Alice"
TEST_CONTACT_ID = 42
TEST_PENDING_CONTACT_ID = "jsTgWcsEQ2g9rnomeK1g/hmO8M1Ix6ZIGWAjgBtlS9U="

TEST_CONTACT_FIRST = {
    "lastChatActivity": 1
}
TEST_CONTACT_SECOND = {
    "lastChatActivity": 2
}
TEST_CONTACT_RESPONSE_SINGLE = [
    TEST_CONTACT_FIRST
]
TEST_CONTACT_RESPONSE_TWO_UNORDERED = [
    TEST_CONTACT_FIRST,
    TEST_CONTACT_SECOND
]
TEST_CONTACT_RESPONSE_TWO_ORDERED = [
    TEST_CONTACT_SECOND,
    TEST_CONTACT_FIRST
]

TEST_LINK = "briar://wvui4uvhbfv4tzo6xwngknebsxrafainnhldyfj63x6ipp4q2vigy"


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.register_uri("POST",
                               BASE_HTTP_URL + "add/pending/",
                               request_headers=request_headers,
                               additional_matcher=match_request_add_pending)
    contacts.add_pending(TEST_LINK, TEST_ALIAS)


def match_request_add_pending(request):
    return {"alias": TEST_ALIAS, "link": TEST_LINK} == request.json()


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending_invalid_public_key(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.post(BASE_HTTP_URL + "add/pending/",
                       status_code=400,
                       json={"error": "INVALID_PUBLIC_KEY"})
    with pytest.raises(PendingContactInvalidPublicKeyException):
        contacts.add_pending(TEST_LINK, TEST_ALIAS)


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending_invalid_link(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.post(BASE_HTTP_URL + "add/pending/",
                       status_code=400,
                       json={"error": "INVALID_LINK"})
    with pytest.raises(PendingContactInvalidLinkException):
        contacts.add_pending(TEST_LINK, TEST_ALIAS)


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending_contact_exists(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.post(BASE_HTTP_URL + "add/pending/",
                       status_code=403,
                       json={
                           "error": "CONTACT_EXISTS",
                           "remoteAuthorName": TEST_ALIAS
                       })
    with pytest.raises(PendingContactAlreadyExistsContact):
        contacts.add_pending(TEST_LINK, TEST_ALIAS)


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending_pending_exists(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.post(BASE_HTTP_URL + "add/pending/",
                       status_code=403,
                       json={
                           "error": "PENDING_EXISTS",
                           "pendingContactAlias": TEST_ALIAS,
                           "pendingContactId": TEST_PENDING_CONTACT_ID
                       })
    with pytest.raises(PendingContactAlreadyExistsPendingContact):
        contacts.add_pending(TEST_LINK, TEST_ALIAS)


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending_unknown_error(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.post(BASE_HTTP_URL + "add/pending/",
                       status_code=500,
                       json={"error": "CRAZY_ERROR"})
    with pytest.raises(BriarWrapperException):
        contacts.add_pending(TEST_LINK, TEST_ALIAS)


@requests_mock.Mocker(kw="requests_mock")
def test_add_pending_no_json(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.post(BASE_HTTP_URL + "add/pending/",
                       status_code=500)
    with pytest.raises(BriarWrapperException):
        contacts.add_pending(TEST_LINK, TEST_ALIAS)


@requests_mock.Mocker(kw="requests_mock")
def test_delete_pending(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.register_uri("DELETE",
                               BASE_HTTP_URL + "add/pending/",
                               request_headers=request_headers,
                               additional_matcher=match_request_delete_pending)
    contacts.delete_pending(TEST_PENDING_CONTACT_ID)


def match_request_delete_pending(request):
    return {"pendingContactId": TEST_PENDING_CONTACT_ID} == request.json()


@requests_mock.Mocker(kw="requests_mock")
def test_delete_pending_unknown_error(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.delete(BASE_HTTP_URL + "add/pending/",
                         status_code=500)
    with pytest.raises(BriarWrapperException):
        contacts.delete_pending(TEST_PENDING_CONTACT_ID)


@requests_mock.Mocker(kw="requests_mock")
def test_set_alias(api, request_headers, requests_mock):
    contacts = Contacts(api)
    contact_id = 137
    requests_mock.register_uri("PUT",
                               BASE_HTTP_URL + f"{contact_id}/alias",
                               request_headers=request_headers,
                               additional_matcher=match_request_set_alias)
    contacts.set_alias(contact_id, TEST_ALIAS)


def match_request_set_alias(request):
    return {"alias": TEST_ALIAS} == request.json()


@requests_mock.Mocker(kw="requests_mock")
def test_delete(api, request_headers, requests_mock):
    contacts = Contacts(api)
    contact_id = 137
    requests_mock.register_uri("DELETE",
                               BASE_HTTP_URL + str(contact_id),
                               request_headers=request_headers)
    contacts.delete(contact_id)


@requests_mock.Mocker(kw='requests_mock')
def test_get(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.register_uri("GET", BASE_HTTP_URL,
                               request_headers=request_headers,
                               text=json.dumps(TEST_CONTACT_RESPONSE_SINGLE))
    assert contacts.get() == TEST_CONTACT_RESPONSE_SINGLE


@requests_mock.Mocker(kw='requests_mock')
def test_get_empty(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.register_uri("GET", BASE_HTTP_URL,
                               request_headers=request_headers,
                               text=json.dumps([]))
    assert contacts.get() == []


@requests_mock.Mocker(kw='requests_mock')
def test_get_unordered(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.register_uri("GET", BASE_HTTP_URL,
                               request_headers=request_headers,
                               text=json.dumps(
                                   TEST_CONTACT_RESPONSE_TWO_UNORDERED)
                               )
    assert contacts.get() == TEST_CONTACT_RESPONSE_TWO_ORDERED


@requests_mock.Mocker(kw='requests_mock')
def test_get_ordered(api, request_headers, requests_mock):
    contacts = Contacts(api)
    requests_mock.register_uri("GET", BASE_HTTP_URL,
                               request_headers=request_headers,
                               text=json.dumps(
                                   TEST_CONTACT_RESPONSE_TWO_ORDERED)
                               )
    assert contacts.get() == TEST_CONTACT_RESPONSE_TWO_ORDERED


@requests_mock.Mocker(kw='requests_mock')
def test_get_link(api, request_headers, requests_mock):
    contacts = Contacts(api)
    response = {"link": TEST_LINK}

    requests_mock.register_uri("GET", BASE_HTTP_URL + "add/link/",
                               request_headers=request_headers,
                               text=json.dumps(response))
    assert contacts.get_link() == TEST_LINK


def test_watch_signal_added(api):
    contacts = Contacts(api)
    contacts._api.socket_listener = SocketListener(api)
    contacts._api.socket_listener._highest_signal_id = 136

    assert contacts._api.socket_listener._signals == dict()

    contacts.watch_connections(None)

    expected_signals = {
        137: {
            "event": "ContactConnectedEvent",
            "callback": contacts.handle_connections_callback
        },
        138: {
            "event": "ContactDisconnectedEvent",
            "callback": contacts.handle_connections_callback
        }
    }

    assert contacts._api.socket_listener._signals == expected_signals


def test_handle_connections_callback_contact_connected(api, mocker):
    contacts = Contacts(api)
    contacts._connections_callback = mocker.MagicMock()

    message = {
        "data": {
            "contactId": TEST_CONTACT_ID
        },
        "name": "ContactConnectedEvent"
    }

    contacts.handle_connections_callback(message)

    contacts._connections_callback.assert_called_once_with(
        TEST_CONTACT_ID, True
    )


def test_handle_connections_callback_contact_disconnected(api, mocker):
    contacts = Contacts(api)
    contacts._connections_callback = mocker.MagicMock()

    message = {
        "data": {
            "contactId": TEST_CONTACT_ID
        },
        "name": "ContactDisconnectedEvent"
    }

    contacts.handle_connections_callback(message)

    contacts._connections_callback.assert_called_once_with(
        TEST_CONTACT_ID, False
    )


def test_handle_connections_callback_wrong_event(api, mocker):
    contacts = Contacts(api)
    contacts._connections_callback = mocker.MagicMock()

    message = {
        "data": {
            "contactId": TEST_CONTACT_ID
        },
        "name": "SomeEvent"
    }

    with pytest.raises(Exception,
                       match="Wrong event in callback: SomeEvent"):
        contacts.handle_connections_callback(message)
