# Copyright (c) 2020 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

import pytest

from briar_wrapper.models.socket_listener import SocketListener

MODULE = "briar_wrapper.models.socket_listener.%s"


def test_init_websocket_thread(api, mocker):
    thread_mock = mocker.patch(MODULE % "Thread")

    SocketListener(api)

    thread_mock.assert_called_once()


def test_init_watch_loop(api, mocker):
    watch_loop_mock = mocker.patch(MODULE % "SocketListener._start_watch_loop")

    SocketListener(api)

    watch_loop_mock.assert_called_once()


def test_connect_lock(api, mocker):
    lock_mock = mocker.Mock()

    manager = mocker.Mock()
    manager.attach_mock(lock_mock.acquire, 'acquire_mock')
    manager.attach_mock(lock_mock.release, 'release_mock')

    socket_listener = SocketListener(api)
    socket_listener._signals_lock = lock_mock

    socket_listener.connect(None, None)

    expected_calls = [
        mocker.call.acquire_mock(),
        mocker.call.release_mock()
    ]

    assert manager.mock_calls == expected_calls


def test_connect_signal_added(api, mocker):
    event_mock = mocker.MagicMock()
    callback_mock = mocker.MagicMock()

    socket_listener = SocketListener(api)
    socket_listener._highest_signal_id = 136

    assert socket_listener._signals == dict()

    socket_listener.connect(event_mock, callback_mock)

    expected_signals = {
        137: {
            "event": event_mock,
            "callback": callback_mock
        }
    }

    assert socket_listener._signals == expected_signals


def test_connect_signal_id(api, mocker):
    event_mock = mocker.MagicMock()
    callback_mock = mocker.MagicMock()

    socket_listener = SocketListener(api)
    socket_listener._highest_signal_id = 137

    signal_id = socket_listener.connect(event_mock, callback_mock)

    assert signal_id == 137 + 1


def test_disconnect_lock(api, mocker):
    lock_mock = mocker.Mock()

    manager = mocker.Mock()
    manager.attach_mock(lock_mock.acquire, 'acquire_mock')
    manager.attach_mock(lock_mock.release, 'release_mock')

    socket_listener = SocketListener(api)
    socket_listener._signals_lock = lock_mock
    socket_listener._signals = mocker.MagicMock()

    socket_listener.disconnect(None)

    expected_calls = [
        mocker.call.acquire_mock(),
        mocker.call.release_mock()
    ]

    assert manager.mock_calls == expected_calls


def test_disconnect_signal_removed(api, mocker):
    socket_listener = SocketListener(api)
    socket_listener._signals = {
        137: {
            "event": None,
            "callback": None
        }
    }

    socket_listener.disconnect(137)

    assert socket_listener._signals == dict()


@pytest.mark.skip(reason="todo")
def test_start_websocket():
    pass


@pytest.mark.skip(reason="todo")
def test_watch_messages():
    pass


def test_call_signal_callbacks(api, mocker):
    message = {
        'name': 'VeryImportantEvent'
    }
    callback_mock = mocker.MagicMock()

    socket_listener = SocketListener(api)
    socket_listener._signals = {
        137: {
            "event": 'VeryImportantEvent',
            "callback": callback_mock
        }
    }

    socket_listener._call_signal_callbacks(message)

    callback_mock.assert_called_once_with(message)


def test_call_signal_callbacks_lock(api, mocker):
    lock_mock = mocker.Mock()

    manager = mocker.Mock()
    manager.attach_mock(lock_mock.acquire, 'acquire_mock')
    manager.attach_mock(lock_mock.release, 'release_mock')

    socket_listener = SocketListener(api)
    socket_listener._signals_lock = lock_mock
    socket_listener._signals = mocker.MagicMock()

    socket_listener._call_signal_callbacks(None)

    expected_calls = [
        mocker.call.acquire_mock(),
        mocker.call.release_mock()
    ]

    assert manager.mock_calls == expected_calls
