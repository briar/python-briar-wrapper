# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

import pytest

from briar_wrapper.api import Api
from briar_wrapper.constants import BRIAR_AUTH_TOKEN, BRIAR_DB

MODULE = "briar_wrapper.api.%s"

HEADLESS_JAR = 'briar-headless.jar'
PASSWORD = 'LjnM6/WPQ]V?@<=$'
CREDENTIALS = ('Alice', PASSWORD)


def test_has_account(mocker):
    isfile_mock = mocker.patch('os.path.isfile')
    isfile_mock.return_value = True
    api = Api(HEADLESS_JAR)

    assert api.has_account() is True
    isfile_mock.assert_called_once_with(BRIAR_DB)


def test_is_running(api_thread):
    api_thread.is_running.return_value = True
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    assert api.is_running() is True
    api_thread.is_running.assert_called_once()


def test_is_not_running(api_thread):
    api_thread.is_running.return_value = False
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    assert api.is_running() is False
    api_thread.is_running.assert_called_once()


def test_login_thread_start(api_thread):
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    api.login(PASSWORD, None)

    api_thread.start.assert_called_once()


def test_login_thread_watch(api_thread, callback):
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    api.login(PASSWORD, callback)

    api_thread.watch.assert_called_once_with(callback)


def test_register_thread_start(api_thread):
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    api.register(CREDENTIALS, None)

    api_thread.start.assert_called_once()


def test_register_thread_watch(api_thread, callback):
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    api.register(CREDENTIALS, callback)

    api_thread.watch.assert_called_once_with(callback)


def test_register_invalid_credentials():
    api = Api(HEADLESS_JAR)

    with pytest.raises(Exception, match="Can't process credentials"):
        api.register(PASSWORD, None)


def test_stop(api_thread):
    api = Api(HEADLESS_JAR)
    api._api_thread = api_thread

    api.stop()

    api_thread.stop.assert_called_once()


def test_successful_startup_socket_listener(callback, mocker):
    socket_listener_mock = mocker.patch(MODULE % "SocketListener")
    isfile_mock = mocker.patch(MODULE % "os.path.isfile")
    isfile_mock.return_value = True
    open_mock = mocker.patch("builtins.open")
    api = Api(HEADLESS_JAR)

    api.on_successful_startup(callback)

    socket_listener_mock.assert_called_once_with(api)


def test_successful_startup_callback(callback, mocker):
    isfile_mock = mocker.patch(MODULE % "os.path.isfile")
    isfile_mock.return_value = True
    open_mock = mocker.patch("builtins.open")
    api = Api(HEADLESS_JAR)

    api.on_successful_startup(callback)

    callback.assert_called_once_with(True)


def test_successful_startup_no_db(mocker):
    isfile_mock = mocker.patch(MODULE % "os.path.isfile")
    isfile_mock.return_value = False
    api = Api(HEADLESS_JAR)

    with pytest.raises(Exception, match="Can't load authentication token"):
        api.on_successful_startup(None)


def test_successful_startup_open_auth_token(callback, mocker):
    socket_listener_mock = mocker.patch(MODULE % "SocketListener")
    isfile_mock = mocker.patch(MODULE % "os.path.isfile")
    isfile_mock.return_value = True
    open_mock = mocker.patch("builtins.open")
    api = Api(HEADLESS_JAR)

    api.on_successful_startup(callback)

    open_mock.assert_called_once_with(BRIAR_AUTH_TOKEN, 'r')


@pytest.fixture
def api_thread(mocker):
    return mocker.patch('briar_wrapper.api.ApiThread')


@pytest.fixture
def callback(mocker):
    return mocker.MagicMock()
