# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

import pytest

# pylint: disable=redefined-outer-name


@pytest.fixture
def api(auth_token, mocker):
    api = mocker.Mock()
    api.auth_token = auth_token
    return api


@pytest.fixture
def auth_token():
    return 'NMDArEDjYlagINGd77WkKdxqDUAkYCWwh'


@pytest.fixture
def request_headers(auth_token):
    request_headers = {
        "Authorization": 'Bearer %s' % auth_token
    }
    return request_headers
