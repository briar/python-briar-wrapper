# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Wrapper around Briar API's _/messages/_ resource
"""

from urllib.parse import urljoin

import requests

from briar_wrapper.constants import BASE_HTTP_URL
from briar_wrapper.model import Model


class PrivateChat(Model):

    _API_ENDPOINT = "messages/"

    def __init__(self, api, contact_id):
        """
        Initialize with `briar_wrapper.api.Api` instance `api` and `contact_id`
        """
        super().__init__(api)
        self._contact_id = contact_id

    def get(self):
        # pylint: disable=line-too-long
        """
        Returns list containing all messages from contact

        [Upstream documentation](https://code.briarproject.org/briar/briar/blob/master/briar-headless/README.md#listing-all-private-messages)

        .. versionadded:: 0.0.3
        """
        url = urljoin(BASE_HTTP_URL,
                      self._API_ENDPOINT + str(self._contact_id))
        request = requests.get(url, headers=self._headers)
        return request.json()

    def send(self, message):
        # pylint: disable=line-too-long
        """
        Sends `message` to contact

        [Upstream documentation](https://code.briarproject.org/briar/briar/blob/master/briar-headless/README.md#writing-a-private-message)

        .. versionadded:: 0.0.3
        """
        url = urljoin(BASE_HTTP_URL,
                      self._API_ENDPOINT + str(self._contact_id))
        requests.post(url, headers=self._headers, json={"text": message})

    def mark_read(self, message_id):
        # pylint: disable=line-too-long
        """
        Marks message as read

        [Upstream documentation](https://code.briarproject.org/briar/briar/blob/master/briar-headless/README.md#marking-private-messages-as-read)

        .. versionadded:: 0.0.5
        """
        url = urljoin(BASE_HTTP_URL,
                      self._API_ENDPOINT + str(self._contact_id) + "/read")
        requests.post(url, headers=self._headers, json={"messageId": message_id})

    def delete_all_messages(self):
        # pylint: disable=line-too-long
        """
        Deletes all messages

        [Upstream documentation](https://code.briarproject.org/briar/briar/-/blob/master/briar-headless/README.md#deleting-all-private-messages)

        .. versionadded:: 0.0.5
        """
        url = urljoin(BASE_HTTP_URL,
                      self._API_ENDPOINT + str(self._contact_id) + "/all")
        requests.delete(url, headers=self._headers)
