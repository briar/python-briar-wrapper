# Copyright (c) 2021 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Thrown when a contact already exists while adding a pending contact
"""
from briar_wrapper.exception import BriarWrapperException


class PendingContactAlreadyExistsPendingContact(BriarWrapperException):

    def __init__(self, response, message=""):
        self.response = response
        error = response.json()
        self.pending_contact_alias = error["pendingContactAlias"]
        self.pending_contact_id = error["pendingContactId"]
        super().__init__(response, message)
