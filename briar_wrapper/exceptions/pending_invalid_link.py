# Copyright (c) 2021 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Thrown when public key of pending contact is invalid
"""
from briar_wrapper.exception import BriarWrapperException


class PendingContactInvalidLinkException(BriarWrapperException):
    pass
