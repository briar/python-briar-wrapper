# Copyright (c) 2021 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Exceptions thrown in case of errors
"""
