# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Abstract base class for all `briar_wrapper.models`
"""


class Model:  # pylint: disable=too-few-public-methods

    _headers = {}

    def __init__(self, api):
        """
        Initialize with `briar_wrapper.api.Api` instance `api`
        """
        self._api = api
        self._initialize_headers()

    def _initialize_headers(self):
        self._headers["Authorization"] = "Bearer %s" % self._api.auth_token
