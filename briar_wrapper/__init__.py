# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

"""
Wrapper for the Briar Headless REST API

Before using `briar_wrapper` you need to initialize an instance of
`briar_wrapper.api.Api`. You can check with `briar_wrapper.api.Api.has_account`
whether you want want to `briar_wrapper.api.Api.login` or
`briar_wrapper.api.Api.register`. Now you can start to use the wrappers in
`briar_wrapper.models`.

The source code can be found on
[code.briarproject.org](https://code.briarproject.org/briar/python-briar-wrapper).
"""

__version__ = "0.0.7"
