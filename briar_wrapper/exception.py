# Copyright (c) 2021 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Forward exceptions thrown by Briar Headless
"""


class BriarWrapperException(Exception):

    def __init__(self, response, message=""):
        self.response = response
        super().__init__(message)
