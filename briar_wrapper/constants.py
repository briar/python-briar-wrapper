# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md
"""
Constants used in `briar_wrapper`
"""

from os.path import join
from pathlib import Path
from urllib.parse import urljoin

_BRIAR_DIR_NAME = ".briar"
_HOME = str(Path.home())
_BRIAR_DIR = join(_HOME, _BRIAR_DIR_NAME)

_HOST = "%s://localhost:7000"
_VERSION_SUFFIX = "v1/"

BRIAR_AUTH_TOKEN = join(_BRIAR_DIR, "auth_token")
"""
Path to Briar's authentication token
"""

BRIAR_DB = join(_BRIAR_DIR, "db", "db.mv.db")
"""
Path to Briar's database
"""

BASE_HTTP_URL = urljoin(_HOST % "http", _VERSION_SUFFIX)
"""
Base URL to construct resource's URLs
"""

WEBSOCKET_URL = urljoin(_HOST % "ws", "%s/ws" % _VERSION_SUFFIX)
"""
Websocket URL used in `briar_wrapper.models.socket_listener.SocketListener`
"""
