#!/usr/bin/env bash
# Copyright (c) 2019 Nico Alt
# SPDX-License-Identifier: AGPL-3.0-only
# License-Filename: LICENSE.md

PYTHONPATH=briar_wrapper pytest --cov-report term-missing --cov=briar_wrapper tests/
