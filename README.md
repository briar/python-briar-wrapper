# Briar Wrapper

**Note: As of Jan 21, 2022, Briar GTK and therefore also _python-briar-wrapper_ is officially deprecated and replaced by [Briar Desktop](https://code.briarproject.org/briar/briar-desktop/).**

A wrapper library for Briar to run on Python platforms.
It uses the
[Briar REST API](https://code.briarproject.org/briar/briar/blob/master/briar-headless/README.md)
and therefore requires Java.

## Installation

The easiest way to install Briar Wrapper is to
install [the package from PyPi.org](https://pypi.org/project/briar_wrapper/):
`pip install briar_wrapper`

## Documentation

The documentation of the project can be found at
[wrapper.docs.briarproject.org](https://wrapper.docs.briarproject.org/).
To generate it locally, call `tools/generate-docs.sh`.

## Building

You can also build it yourself by runninng the following command:
`flit build`

Note that you need to [have installed flit](https://pypi.org/project/flit/) to
do this.

## Debian

First, install some dependencies:
```bash
sudo apt install build-essential devscripts debhelper python3-all flit dh-python
```

During building the package, we will also run tests:
```bash
sudo apt install python3-pytest python3-requests python3-websockets python3-requests-mock python3-pytest-mock
```

In order to generate the original source package, install flit and use it to build
python-briar-wrapper:
```bash
flit build
```

You can now use the archive generated in the _dist_ directory to build the Debian
package:
```bash
mv dist/briar_wrapper-0.0.3.tar.gz ../python-briar-wrapper_0.0.3.orig.tar.gz
cd ..
tar -zxf python-briar-wrapper_0.0.3.orig.tar.gz
cd briar_wrapper-0.0.3
```

Now, build the .deb like this:
```bash
debuild -us -uc
```

And install the .deb like this:
```bash
sudo dpkg -i ../python3-briar-wrapper_0.0.3-1_all.deb
```

## Design Goals

* Main platform is GNU/Linux, but should also support (at least) Windows and macOS
* Analogously, main platform is x86, but should also support (at least) arm

## FAQ

### I forgot my password.

In case you forgot your password, there's no other way than to delete your old
account and start from scratch. Call `rm -rf ~/.briar/` to delete your current
account.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU Affero General Public License](LICENSE.md) for more details.
